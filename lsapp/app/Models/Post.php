<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class POST extends Model
{
    use HasFactory;
    
    // table name
    protected $table = 'posts';
    //Primary Key
    public $primarykey = 'id';
    //Timestamps
    public $timestamps = true;

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

}
